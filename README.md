# Imagen-link-prompt


## Use case demonstrated:
A demo illustrates how content creators and marketing teams can use existing videos as a source of inspiration for their own creative work or edit as needed. It showcases how easy it is to draw upon existing visual content and use it as a starting point for generating new and original videos.

## The workflow demonstrated is as follows:
A content creator uploads a video to Gemini 1.5 Pro to extract detailed information about the image that the user may not have realized or expressed.
Gemini 1.5 Pro utilizes a base prompt, similar to the one provided below, to generate a prompt and negative prompts (in JSON format) based on a video. These prompts can then be used as input prompts for the Imagen 2 model.

## Base prompt: 
I want a video like this. Write a prompt that I can pass to the imagen2 model to generate a video like this.
Estimate camera moments, focal length and picture metadata from the video and include that as part of the prompt generated.
Output should just contain prompt information as json with each prompt and its associated negative prompt.
Do not include headings or explanations in output.
Example Output should look exactly like this -
{
\"prompt\": \"Camera dolly, revealing mountains, cold weather, b-roll, dynamic",
\"negative_prompt\": \"cartoon, painting, blurry, amateur\"
}
The Imagen 2 model uses the generated prompts (in the json response above) as input. These prompts give the model direction and background, allowing it to make live-images that match the desired content and style with watermark.

This demo illustrates how users can inspire from any video of their choice and create new and unique video content as required. This can work with image also. 
